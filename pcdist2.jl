include("duraccent.jl")
include("ismonophonic.jl")
using DigitalMusicology
using DataFrames

function pcdist2(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(nframe)
        throw(ArgumentError("frame is not monophonic"))
    end
    pcd = zeros(12,12)
    pc = map(e->Int64(PitchOps.pc(e))+1,nframe[:pitch])
    du = duraccent.(dur(nframe,"secs"))
    for k in 2:length(pc)
        pcd[pc[k-1],pc[k]] += du[k-1]*du[k]
    end
    return pcd/(sum(pcd) + 1e-12)
end

function pitchClassDistribution2(notes,weight = (e1,e2)-> e1 + e2)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame is not monophonic"))
    end
    pcd = Dict()
    for k = 1:length(notes)-1
        (p1,p2) = pitch(notes[k]),pitch(notes[k+1])
        if !haskey(pcd,(p1,p2))
            pcd[(p1,p2)] = 0
        end
        pcd[(p1,p2)] +=  weight(duration(notes[k]),duration(notes[k+1]))
    end
    sv = sum(values(pcd))
    if sv == 0
        return pcd
    end
    for k in keys(pcd)
        pcd[k] = pcd[k]/sv
    end
    return pcd
end
#test
frame1 = midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
pitchClassDistribution2(dfToNotes(frame1,"secs"),(e1,e2)->(duraccent(e1) + duraccent(e2)))
