include("dur.jl")
using DataFrames
using DigitalMusicology
using StatsBase
using Ratios
function ismonophonic(nframe :: DataFrame,overlap :: Union{Float64,SimpleRatio{Int64}} = 0.1, timetype :: String = "secs")
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end

    ismono = true
    i = 1
    ccol = 1
    nrow,ncol = size(nframe)
    if timetype == "secs"
        ccol = 5
    elseif timetype =="wholes"
        ccol = 3
    elseif timetype == "ticks"
        ccol = 1
    else
        throw(ArgumentError("invalid timetype"))
    end
    while ismono && i != nrow
        if nframe[i + 1,ccol]-nframe[i,ccol+1] < -overlap
            ismono = false
        end
        i += 1
    end
    return ismono
end

"""
    here the notes have to be in the correct order, beware of the type of the overlap and the time type of the notes
"""
function ismonophonic(notes,overlap :: Union{Int64,Float64,SimpleRatio{Int64}} = 0.1)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    ismono = true
    i = 1

    while ismono && i != length(notes)
        if onset(notes[i+1])- offset(notes[i]) < -overlap
            ismono = false
        end
        i += 1
    end
    return ismono
end
#test
frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/laksin.mid")
ismonophonic(dfToNotes(frame1,"secs"))
