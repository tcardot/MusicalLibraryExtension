include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
include("gettempo.jl")
using DataFrames
using DigitalMusicology.MidiFiles
function settempo(nframe :: DataFrame, bpm :: Int64 )
    ratio = gettempo(nframe)/bpm
    rframe = DataFrame(nframe)
    rframe[:onset_secs] *= ratio
    rframe[:offset_secs] *= ratio
    return rframe
end
