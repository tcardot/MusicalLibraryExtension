include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DigitalMusicology.MidiFiles
using StatsBase.crosscor

function ofacorr(of)
    MAXLAG = 8
    NDIVS = 4
    actmp = crosscor(of,of)
    ind1 = Int64((length(actmp)+1)/2)
    ind2 = Int64(min(length(actmp),ind1 + MAXLAG*NDIVS))
    ac = zeros(MAXLAG*NDIVS + 1)
    ac[1:ind2-ind1+1] = actmp[ind1:ind2]
    if ac[1] > 0
        ac = ac/ac[1]
    end
    return ac[3:2:end]
end
frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/laksin.mid")
ofacorr(frame1[:onset_secs])
#frame1
