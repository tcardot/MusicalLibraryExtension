include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DataFrames

function gettempo(nframe :: DataFrame)
    return 240*Float64(nframe[1,:offset_wholes]-nframe[1,:onset_wholes])/(nframe[1,:offset_secs]-nframe[1,:onset_secs])
end
