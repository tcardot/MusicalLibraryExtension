using DigitalMusicology

function refstat(stat :: Symbol)
    if stat == Symbol("kkmaj")

#Psychological data (K&K major profile)
        ref=[6.35,2.23,3.48,2.33,4.38,4.09,2.52,5.19,2.39,3.66,2.29,2.88]
        label = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]

    end

    if stat == :kkmin
#Psychological data (K&K minor profile)
    	ref=[6.33,2.68,3.52,5.38,2.6,3.53,2.54,4.75,3.98,2.69,3.34,3.17]
    	label = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]
    	return (ref,label)
    end

    if stat == :kkmajt

# Psychological data revised (Temperley major profile)
# Temperley, D. (1999). What's Key for Key? The Krumhansl-Schmuckler Key-Finding
# Algorithm Reconsidered. Music Perception, 17, 65-100.
        ref=[5,2,3.5,2,4.5,4,2,4.5,2,3.5,1.5,4];
    	label = {"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"}
        return (ref,label)
    end

end
