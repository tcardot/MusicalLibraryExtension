include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DigitalMusicology
using DataFrames
using DigitalMusicology.MidiFiles

function dur(nframe :: DataFrame, timetype  :: String = "wholes" )
  if isempty(nframe)
      throw(ArgumentError("Null argument"))
  end

  if(timetype == "ticks")
    return nframe[:offset_ticks]-nframe[:onset_ticks]
  elseif timetype == "secs"
    return nframe[:offset_secs]-nframe[:onset_secs]
  elseif timetype == "wholes"
    return nframe[:offset_wholes]-nframe[:onset_wholes]
  else
    throw(ArgumentError("invalid timetype"))
  end
end

#frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
#show(dur(frame1, "wholes"))
