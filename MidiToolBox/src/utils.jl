
import Base: iterate, IteratorSize, IteratorEltype, length, HasLength, EltypeUnknown
using Base.Iterators

export Itermidi,noteSequence, mchannels, dfToNotes, ismonophonic, duraccent


struct Itermidi
    midiframe :: DataFrame
    timetype :: String
end

frame1 = midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sarabande.mid")
function iterate(iter ::Itermidi, state :: Int64 = 0)
    if state == size(iter.midiframe,1)
        return nothing
    end
    return (TimedNote(iter.midiframe[state+1,:pitch],iter.midiframe[state+1,Symbol("onset_",iter.timetype)],iter.midiframe[state+1,Symbol("offset_",iter.timetype)]),state +1)
end

IteratorSize(Itermidi) = HasLength()
IteratorEltype(Itermidi) = EltypeUnknown()
length(iter :: Itermidi) = size(midiframe,1)

function weightnotes(notes, weight = e->e)
    return map(weight,notes)
end
function combinenotes(notes,op)
end
function noteSequence(notes,n :: Int64)
    seq = zeros(n)
    for i = 1:n
        seq[n] = drop(notes,i-1)
    end
    return zip(seq)
end



"""
return the set of channels appearing in the dataframe
"""
function mchannels(nframe :: DataFrame)
    if isempty(nframe)
        return []
    end
    chn = fill(-1,16)
    foreach(e-> chn[e+1] = e, nframe[:channel])
    return filter(e-> e != -1,chn)
end



"""
    dfToNotes(nframe :: DataFrame,timetype, channels = mchannels(nframe))

convert a dataframe of midi notes to an array of Notes
"""
function dfToNotes(nframe :: DataFrame,timetype, channels = mchannels(nframe))
    if isempty(nframe)
        return []
    end
    p = nframe[:pitch]
    notes= []
    for k = 1:length(p)
        #=if !(nframe[k,:channel] in channels)
            continue
        end =#
        push!(notes,TimedNote(p[k],nframe[k,Symbol("onset_",timetype)],nframe[k,Symbol("offset_",timetype)]))
    end
    return notes
end


"""
     ismonophonic(notes,overlap :: Union{Int64,Float64,SimpleRatio{Int64}} = 0.1)

verify if the the set of notes is monophonic
"""
function ismonophonic(notes,overlap :: Union{Int64,Float64,SimpleRatio{Int64}} = 0.1)
    if isempty(notes)
        return true
    end
    if overlap < 0
        throw(ArgumentError("overlap must be positive"))
    end
    ismono = true
    i = 1

    while ismono && i != length(notes)
        if onset(notes[i+1])- offset(notes[i]) < -overlap
            ismono = false
        end
        i += 1
    end
    return ismono
end

function duraccent(dur :: Union{Float64,Int64},tau :: Float64 = 0.5, accentIndex :: Float64 = 2.0)
  return (1-exp(-dur/tau))^accentIndex
end
