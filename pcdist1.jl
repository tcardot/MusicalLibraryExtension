
include("duraccent.jl")
include("dur.jl")
include("interfaceFunctions.jl")
using DataFrames
using DigitalMusicology
using Base.Dict

function pcdist1(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end
    pcd = zeros(12)
    #b =zip(nframe[:pitch],duraccent.(dur(nframe,"secs")))
    #println(b[4])
    foreach(p-> pcd[Int64(pc(p.first))+1] += p.second,Pair.(nframe[:pitch],duraccent.(dur(nframe,"secs"))))
    return pcd/(sum(pcd) + 1e-12)
end

function pitchClassDistribution(notes,weight = e ->e)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    d = Dict{Any,Any}()
    for note in notes
        key = pc(pitch(note))
        if !haskey(d,key)
            d[key] = 0
        end
        d[key] += weight(duration(note) )
    end

    sv= sum(values(d))
    if sv == 0
        return d
    end
    for k in keys(d)
        d[k] = d[k]/sv
    end
    return d

end

frame1 = midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
pitchClassDistribution(dfToNotes(frame1,"secs"),duraccent)
