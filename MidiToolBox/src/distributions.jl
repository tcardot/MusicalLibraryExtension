
export pitchClassDistribution, pitchClassDistribution2,
durationDistribution,durationDistribution2,
intervalDistribution,intervalDistribution2,
keysom
"""
    pitchClassDistribution(notes,weight = e ->e)

compute the pitchclass distribution. Notes are weighted by their duration passed to the "weight" function
return a Dictionary pitch=>proportion
"""
function pitchClassDistribution(notes,weight = e ->e)
    if iterate(notes) == nothing
        return Dict()
    end
    d = Dict{Pitch,Any}()
    for note in notes
        key = pc(pitch(note))
        if !haskey(d,key)
            d[key] = 0
        end
        d[key] += weight(duration(note) )
    end

    sv= sum(values(d))
    if sv == 0
        return d
    end
    for k in keys(d)
        d[k] = d[k]/sv
    end
    return d

end

"""
    pitchClassDistribution2(notes,weight = (e1,e2)-> e1 + e2)

compute the second-order pitchclass distribution.
return a Dictionary (pitch1,pitch2)=>proportion
"""
function pitchClassDistribution2(notes,weight = (e1,e2)-> e1 + e2)
    if isempty(notes)
        return Dict()
    end
    if !ismonophonic(notes)
        throw(ArgumentError("notes must be monophonic"))
    end
    pcd = Dict()
    for k = 1:length(notes)-1
        (p1,p2) = pc(pitch(notes[k])),pc(pitch(notes[k+1]))
        if !haskey(pcd,(p1,p2))
            pcd[(p1,p2)] = 0
        end
        pcd[(p1,p2)] +=  weight(duration(notes[k]),duration(notes[k+1]))
    end
    sv = sum(values(pcd))
    if sv == 0
        return pcd
    end
    for k in keys(pcd)
        pcd[k] = pcd[k]/sv
    end
    return pcd
end
"""
    durationDistribution(notes,weight = e->e)

Compute the duration distribution. Notes are weighted by their duration passed to the "weight" function
return a Dictionary duration=>proportion

"""
function durationDistribution(notes,weight = e->e)
  if (iterate(notes))[1] == nothing
    return Dict()
  end
  d = Dict()
  for note in notes
    dur = duration(note)
    if !haskey(d,dur)
      d[dur] = 0
    end
    d[dur] += weight(dur)
  end
  #show(d)
  sv = sum(values(d))
  if sv == 0
    return d
  end
  for k in keys(d)
      d[k] = d[k]/sv
  end
  return d
end

"""
    durationDistribution2(notes, weight = e-> e)

compute the second order duration distribution. Pair of durations (dur1,dur2) are weighted by their number of occurencies
return a Dictionary (dur1,dur2)=>proportion
"""
function durationDistribution2(notes, weight = (e1,e2)-> e1 + e2)
    if isempty(notes)
        return Dict()
    end
    if !ismonophonic(notes)
        throw(ArgumentError("notes must be monophonic"))
    end

    d = Dict()
    for k = 1:length(notes)-1
      (dur1,dur2) = weight(duration(notes[k]),duration(notes[k+1]))
      if(dur1 == 0)
        continue
      end
      if !haskey(d,(dur1,dur2))
        d[(dur1,dur2)] = 0
      end
      d[(dur1,dur2)] += 1
    end
    sv = sum(values(d))
    if sv == 0
      return d
    end
    for k in keys(d)
        d[k] = d[k]/sv
    end
    return d
end

"""
    intervalDistribution(notes, weight = e->e)

Compute the interval distribution. intervals are weighted by the duration of the 2 notes passed to the "weight" function
return a Dictionary pitch=>proportion
"""
function intervalDistribution(notes, weight = (e1,e2)->e1 + e2)
    if isempty(notes)
        return Dict()
    end
    if !ismonophonic(notes)
        throw(ArgumentError("notes must be monophonic"))
    end
    if length(notes) <2
        throw(ArgumentError("need at least 2 notes"))
    end
    ivd = Dict{Pitch,Any}()
    for k = 1:length(notes)-1
        iv = pitch(notes[k+1])-pitch(notes[k])
        if !haskey(ivd,iv)
            ivd[iv] = 0
        end
        ivd[iv] += weight(duration(notes[k]), duration(notes[k+1]))
    end
    sv = sum(values(ivd))
    if sv == 0
        return ivd
    end
    for k in keys(ivd)
        ivd[k] = ivd[k]/sv
    end
    return ivd
end
"""
     intervalDistribution2(notes, weight = (e1,e2)->e1 + e2)

Compute the second-order interval distribution. intervals are weighted by the duration of the 2 notes passed to the "weight" function
return a Dictionary (pitch1,pitch2)=>proportion
"""
function intervalDistribution2(notes, weight = (e1,e2,e3)->e1 + e2 + e3)
    if isempty(notes)
        return Dict()
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame is not monophonic"))
    end
    if length(notes) < 3
        throw(ArgumentError("need at least 3 notes"))
    end

    ivd = Dict()
    for k in 1:length(notes)-2
        (iv1,iv2) = pitch(notes[k+1])- pitch(notes[k]),pitch(notes[k+2])- pitch(notes[k+1])
        if !haskey(ivd,(iv1,iv2))
            ivd[(iv1,iv2)] = 0
        end
        ivd[(iv1,iv2)] += weight(duration(notes[k]),duration(notes[k+1]),duration(notes[k+2]))
    end
    sv = sum(values(ivd))
    if sv == 0
        return ivd
    end
    for k in keys(ivd)
        ivd[k] = ivd[k]/sv
    end
    return ivd
end


function keysom(notes)
    if isempty(notes)
        return []
    end
    z = zeros(24,36)
    somw = read(matopen("C:/Users/Toussain/Documents/DCML/keysomdata.mat"),"somw")
    pcd = pitchClassDistribution(notes,duraccent)

    vtmp = collect(values(pcd))
    ts = mean(vtmp)
    tm = sqrt(sum(vtmp.^2))

    for i in keys(pcd)
        pcd[i] = (pcd[i]-ts)/tm
    end
    for k = 1:36
        for l = 1:24
            s = 0.0
            for m = 1:12
                s += get!(pcd,midi(m),0)*somw[m,k,l]
            end
            z[l,k] = s
        end
    end
    return z
end
