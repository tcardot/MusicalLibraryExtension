include("dur.jl")
using DataFrames
using DigitalMusicology
using StatsBase

export durdist1

function durdist1(nframe :: DataFrame)
  if isempty(nframe)
    throw(ArgumentError("Null argument"))
  end
  durA = dur(nframe)
  filter(e->e > 0, durA)
  durA = map(e-> round(Int64,2*log2(Float64(4*e))) + 5,durA)
  filter(e-> 1<e<9 ,durA)
  if isempty(durA)
    return Array(1:9)
  else

  end
  return StatsBase.fit(Histogram,durA,1:9)
end

"""
only works with timetype "wholes"
"""
function durationDistribution(notes,weight = e->e)
  if isempty(notes)
    throw(ArgumentError("Null argument"))
  end
  d = Dict()
  for note in notes
    dur = duration(note)
    if(dur == 0)
      continue
    end
    if !haskey(d,dur)
      d[dur] = 0
    end
    d[dur] += weight(dur)
  end
  sv = sum(values(d))
  if sv == 0
    return d
  end
  for k in keys(d)
      d[k] = d[k]/sv
  end
  return d
end


#test
frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
durationDistribution(dfToNotes(frame1,"secs"))
