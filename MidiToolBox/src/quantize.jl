
function quantize(notes,tresh = 1//8 )
    nnotes = []
    for note in notes
        push!(nnotes,Note(pitch(note),round(onset(note)/tresh)*tresh,round(offset(note)/tresh)*tresh))
    end
    return nnotes
end
