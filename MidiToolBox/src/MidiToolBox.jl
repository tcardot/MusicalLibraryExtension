module MidiToolBox
using DigitalMusicology
using DataFrames
using Ratios
using MAT
using StatsBase

export  quantize

include("distributions.jl")
include("utils.jl")
include("quantize.jl")




end
