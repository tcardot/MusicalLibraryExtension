using DigitalMusicology

function ivdirdist1(notes)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame is not monophonic"))
    end
    # impossible to implement the generalized version yet
end
