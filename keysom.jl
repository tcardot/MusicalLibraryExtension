using DigitalMusicology
using StatsBase
using Plots
using PyPlot
include("interfaceFunctions.jl")
include("pcdist1.jl")

function keysom(notes)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    z = zeros(24,36)
    somw = read(matopen("C:/Users/Toussain/Documents/DCML/keysomdata.mat"),"somw")
    pcd = pitchClassDistribution(notes,duraccent)
    vtmp = collect(values(pcd))
    ts = mean(vtmp)
    tm = sqrt(sum(vtmp.^2))
    for i in keys(pcd)
        pcd[i] = (pcd[i]-ts)/tm
    end
    show(pcd)
    for k = 1:36
        for l = 1:24
            s = 0.0
            for m = 1:12
                s += get!(pcd,midi(m),0)*somw[m,k,l]
                #show(s)
            end
            z[l,k] = s
        end
    end
    return z
end
#frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sarabande.mid")
#zdata = keysom(dfToNotes(frame1,"secs"))

#data = Dict("z" => zdata, "type" => "heatmap")
#Plots.plot(data,linewidth = 2, title = "oui")
#pyplot()
#heatmap(zdata)
#colorbar()
