

export Itermidi,notesequence, mchannels, ismonophonic, duraccent, quantize,midipitchname

"""
    Itermidi

Iteror over DataFrames representing midi files. The iterator returns a TimedNote
"""
struct Itermidi
    midiframe :: DataFrame
    timetype :: String
end


function iterate(iter ::Itermidi, state :: Int64 = 0)
    if state >= size(iter.midiframe,1)
        return nothing
    end
    return (TimedNote(iter.midiframe[state+1,:pitch],iter.midiframe[state+1,Symbol("onset_",iter.timetype)],iter.midiframe[state+1,Symbol("offset_",iter.timetype)]),state +1)
end

IteratorSize(::Type{Itermidi}) = HasLength()
IteratorEltype(::Type{Itermidi}) = HasEltype()
length(iter :: Itermidi) = size(iter.midiframe,1)


"""
    notesequence(notes,n :: Int64)

return an iterator over all sequences of 'n' consecutive notes
"""
function notesequence(notes,n :: Int64)
    seq = []
    for i = 1:n
        push!(seq,drop(notes,i-1))
    end
    return map(e->collect(e),zip(seq...))
end


"""
    ismonophonic(notes,overlap :: Union{Int64,Float64,Rational{Int64}} = 0.1)

return true if the given notes are monophonic else false.
"""
function ismonophonic(notes,overlap = 0.1)
    if overlap < 0
        throw(ArgumentError("overlap must be positive"))
    end
    ismono = true
    i = 1
    prev = iterate(notes)
    next = (prev == nothing) ? nothing : iterate(notes,prev[2])
    while ismono && next != nothing

        if onset(next[1])- offset(prev[1]) < -overlap
            ismono = false
        end
        prev = next
        next = iterate(notes,prev[2])
    end
    return ismono
end


"""
    quantize(notes,tresh = 1//8 )

quantize the given notes on a grid which has "cells" of length 'thresh'
Caution : sometime the resulting onsets and offsets can have rounding problems (eg. 40.400000000006)
"""
function quantize(note,tresh = 1//8 )

    return TimedNote(pitch(note),round(Int64,onset(note)/tresh)*tresh,round(Int64,offset(note)/tresh)*tresh)
end


"""
    midiPitchName(p :: MidiPitch)

return the name of the given MidiPitch
midi pitches must take values between 21 and 108
"""
function midipitchname(p :: MidiPitch)
  if(p.pitch < 21 || p.pitch > 108)
    throw(ArgumentError("pitch out of bounds"))
  end
  miditable = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]
  return string(miditable[mod(p.pitch,12)+1],floor(Int,(p.pitch)/12)-1)
end


"""
    duraccent(dur :: Int64,tau :: Float64 = 0.5, accentIndex :: Float64 = 2.0)

return the duration of the note corrected by the Parncutt durational accent model
"""
function duraccent(note,tau :: Float64 = 0.5, accentIndex :: Float64 = 2.0)
  return (1-exp(-duration(note)/tau))^accentIndex
end
