include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
include("ismonophonic.jl")
include("duraccent.jl")
using DataFrames
using DigitalMusicology.MidiFiles
using Plots

function ivdist2(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(nframe)
        throw(ArgumentError("frame is not monophonic"))
    end

    pcdiff = diff(Int64.(nframe[:pitch]))
    iv = vcat([0],pcdiff)
    iv = map(e-> mod(abs(e),12)*sign(e)+13,iv)
    println(iv)
    ivd = zeros(25,25)
    du = duraccent.(dur(nframe,"secs"))
    for k in 2:length(iv)
        ivd[iv[k-1],iv[k]] += du[k-1] + du[k]
    end
    return ivd/(sum(ivd)+ 1e-12)
end

function intervalDistribution2(notes, weight = (e1,e2)->e1 + e2)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame is not monophonic"))
    end
    if length(notes) < 3
        throw(ArgumentError("need at least 2 notes"))
    end

    ivd = Dict()
    for k in 1:length(notes)-2
        (iv1,iv2) = pitch(notes[k+1])- pitch(notes[k]),pitch(notes[k+2])- pitch(notes[k+1])
        if !haskey(ivd,(iv1,iv2))
            ivd[(iv1,iv2)] = 0
        end
        ivd[(iv1,iv2)] += weight(duration(notes[k]),duration(notes[k+1]))
    end
    sv = sum(values(ivd))
    if sv == 0
        return ivd
    end
    for k in keys(ivd)
        ivd[k] = ivd[k]/sv
    end
    return ivd
end

frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
#ivdist2(frame1)
#pcdiff = diff(Int64.(frame1[:pitch]))
#iv = vcat([0],pcdiff)
#map(e-> mod(abs(e),12)*sign(e)+13,iv)
#show(iv)
intervalDistribution2(dfToNotes(frame1,"secs"))
