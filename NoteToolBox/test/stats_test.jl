using DigitalMusicology
using NoteToolBox
using Test





@testset "dist" begin
    ea = []
    r = 1:4
    @test dist(ea) == Dict()
    @test dist(r) == Dict([1=>0.25,2=>0.25,3=>0.25,4=>0.25])
    @test dist(r,e->e,e->1,false) == Dict([1=>1,2=>1,3=>1,4=>1])
    @test dist(r,e->mod(e,2)) == Dict([0=>0.5,1=>0.5])
    @test dist(r,e->e,e->e) == Dict([1=>0.1,2=>0.2,3=>0.3,4=>0.4])

    sframe = midifilenotes("test/sample1.mid")
    siter = Itermidi(sframe,"wholes")

    @test pcdist1(siter) == Dict([midi(0)=>0.25,midi(2)=>0.25,midi(4)=>0.25,midi(7)=>0.25])
    #@test pcdist2(siter) == Dict{Any,Any}([(midi(0),midi(2))=>0.333333,(midi(2),midi(4))=>0.333333,(midi(4),midi(7))=>0.333333])  aproximative test, fails for no apparent reason
    @test durdist1(siter) == Dict([1//4 => 1])
    @test durdist2(siter) == Dict([(1//4,1//4) =>1])
    #@test ivdist1(siter) == Dict([2=> 0.666667,3=> 0.333333])
end

@testset "melcontour" begin

    oneframe = midifilenotes("test/onenote.mid")
    lframe = midifilenotes("test/laksin.mid")
    ea = []
    oiter = Itermidi(oneframe,"wholes")
    liter = Itermidi(lframe,"wholes")
    pit(e) = (pitch(e)).pitch
    @test melcontour(ea,1//4,pit) == []
    @test melcontour(oiter,1//4,pit) == [62,62,62,62]
    @test melcontour(oiter,1//8,pit) == [62,62,62,62,62,62,62]
    @test melcontour(liter,1//4,pit) == [64, 71, 71, 67,64,	66,	67,	66,	66,	64,	71,	71,	67,	67,	64,	64,	64,	64]
    #@test melcontour(liter,1//16 ) == [64,64,71, 71, 71, 71,71,	71,	69,	69,	67,	67,	66,	66,	64,	64,	64,	64,	66,	66,	66,	66,	67,	67,	67,	67,	66,	66,	66,	66,	66,	66,	66,	66,	64,	64,	67,	67,	71,	71,	71,	71,	71,	71,	69,	69,	67,	67,	66,	66,	67,	67,	66,	66,	64,	64,	63,	63,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64]
    @test melcontour(liter,1//16,pit) == [64,	64,	64,	64,	71,	71,	71,	71,	71,	71,	69,	69,	67,	67,	66,	66,	64,	64,	64,	64,	66,	66,	66,	66,	67,	67,	67,	67,	66,	66,	66,	66,	66,	66,	66,	66,	64,	64,	67,	67,	71,	71,	71,	71,	71,	71,	69,	69,	67,	67,	66,	66,	67,	67,	66,	66,	64,	64,	63,	63,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64,	64]
end

@testset "acorr" begin
    eframe = midifilenotes("test/empty.mid")
    eiter = Itermidi(eframe,"wholes")
    oframe = midifilenotes("test/onenote.mid")
    oiter = Itermidi(oframe,"wholes")
    @test acorr(eiter,1//4,e->(pitch(e)).pitch) == []
    # Caution : NaN values can be return by the autocor function as in Matlab
    #comparing values from Julia and Matlab is hard as the rounding is not the same, arrays below show the difference
    # Matlab :[1.0, 0.3236, -0.156, -0.2006, -0.1442, -0.0878, -0.2835, -0.1884, 0.1474, 0.3814, 0.1487, -0.0137, -0.0293, -0.146, -0.137, -0.128, -0.0382, 0.0517]
    #Julia : [1.0, 0.32357, -0.156024, -0.200644, -0.14421, -0.0877758, -0.283538, -0.18844, 0.147432, 0.381371, 0.148701, -0.0136692, -0.0292912, -0.145968, -0.136985, -0.128002, -0.0381761, 0.0516501]
end

@testset "mobility" begin

    lframe = midifilenotes("test/laksin.mid")
    liter = Itermidi(lframe,"wholes")
    ea = []
    @test mobility(ea) == []
    show(mobility(liter))
    #test with laksin.mid, length of arrays are equal
    #Julia  [0, 0.0, 0, 0.166667, 0.386322, 0.263754, 1.7454, 0.852011, 0.184192, 0.765752, 1.8761, 0.110236, 2.70097, 2.01592, 0.955004, 0.35166, 0.915004, 0.240131, 0.826335, 1.93549, 2.4542, 1.86349, 1.92259]
    #Matlab [0,0,0,0.1667,0.3863,0.2638,1.7454,0.8520,0.1842,0.7658,1.8761,0.1102,2.7010,2.0159,.9550,0.3517,0.9150,0.2401,0.8263,1.9355,2.4542,1.8635,1.9226]
end

@testset "melaccent" begin
    ea = []
    #@test melaccent(ea) == []
    lframe = midifilenotes("test/laksin.mid")
    liter = Itermidi(lframe,"wholes")
    # MatLab [1,1,1.0000e-05,0.5000,0.2500,0.2500,0.3550,0.0957,0.5561,0.0850,0.3550,0.0957,0.6700,1.0000e-05,0.5000,0.2500,0.3550,0.2407,0.0850,0.2500,0.3550,0.2900,0]
    # Julia  [1.00,1.00,0.0000100,0.500,0.250,0.250,0.355,0.0957,0.556…,0.0850,0.355,0.0957,0.670,0.0000100,0.500,0.250,0.355,0.241…,0.0850,0.250,0.355,0.290,0.00]

end

#acorr(liter,1//4,e->(pitch(e)).pitch)
lframe = midifilenotes("test/laksin.mid")
liter = Itermidi(lframe,"wholes")
melaccent(liter)
#plot(acorr(liter,1//4,e->(pitch(e)).pitch,true))
