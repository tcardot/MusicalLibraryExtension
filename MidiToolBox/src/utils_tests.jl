include("MidiToolbox.jl")
using Base.Test
using DataFrames
using DigitalMusicology

#using MidiToolbox

#chframe = midifilenotes("multich.mid")
chframe = midifilenotes("/Users/Toussain/Documents/DCML/MusicalLibraryExtension/MidiToolBox/src/multich.mid")
frame1 = midifilenotes("/Users/Toussain/Documents/DCML/MusicalLibraryExtension/FunctionTest/sample1.mid")
@testset "utils" begin
    @testset "mchannels" begin
        @test mchannels(DataFrame()) == []  #empty frame
        @test mchannels(frame1) == [0] #basic midi file
        @test mchannels(chframe) == [i for i in 0:15]   #all channels
    end
    @testset "dfToNotes" begin
        @test dfToNotes(DataFrame(),"secs") == [] #empty frame
        for s in ["secs","wholes","ticks"],i = 1:4 # all notes and all time types for 4-notes dataframe
            @test onset(dfToNotes(frame1,s)[i]) == frame1[i,Symbol("onset_",s)]
            @test offset(dfToNotes(frame1,s)[i]) == frame1[i,Symbol("offset_",s)]
        end
    end
    @testset "ismonophonic" begin
        @test ismonophonic([]) == true #empty set
        @test ismonophonic(dfToNotes(frame1,"secs")) == true #true for mono midi
        @test ismonophonic(dfToNotes(frame1,"wholes")) == true
        @test ismonophonic(dfToNotes(frame1,"ticks")) == true
        @test_throws ArgumentError ismonophonic(dfToNotes(frame1,"secs"),-0.1) #fails on negative overlap
        @test ismonophonic(dfToNotes(chframe,"ticks")) == false
        @test ismonophonic(dfToNotes(chframe,"wholes")) == false
        @test ismonophonic(dfToNotes(chframe,"secs")) == false #false for poly midi
        @test ismonophonic(dfToNotes(chframe,"secs"),1) == false #changing overlap

    end
    @testset "duraccent" begin
        @test duraccent(0) == 0
        @test duraccent(0.0) == 0
        @test duraccent(2) < 1 #aproximative test
        @test duraccent(1) > 0.6
    end
end
