using DigitalMusicology
using DataFrames
using MAT
include("mchannels.jl")

function dfToNotes(nframe :: DataFrame,timetype, channels = mchannels(nframe))
    p = nframe[:pitch]
    notes= []
    for k = 1:length(p)
        #=if !(nframe[k,:channel] in channels)
            continue
        end =#
        push!(notes,TimedNote(p[k],nframe[k,Symbol("onset_",timetype)],nframe[k,Symbol("offset_",timetype)]))
    end
    return notes
end
frame1 = midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
dfToNotes(frame1,"secs")
read(matopen("C:/Users/Toussain/Documents/DCML/keysomdata.mat"),"somw")
