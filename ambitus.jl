using DigitalMusicology
using DataFrames

"""
    ambitus(notes)  works only with numerical pitches as the method uses minimum() and maximum()
"""
function ambitus(notes)
    if isempty(notes)
        return 0
    end
    p = pitch.(notes)
    return maximum(p)-minimum(p)
end

#test
#frame1 = midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
#ambitus(dfToNotes(frame1,"secs"))
