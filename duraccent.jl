#include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")

function duraccent(dur :: Float64,tau :: Float64 = 0.5, accentIndex :: Float64 = 2.0)
  return (1-exp(-dur/tau))^accentIndex
end
function duraccent(dur :: Int64,tau :: Float64 = 0.5, accentIndex :: Float64 = 2.0)
  return (1-exp(-dur/tau))^accentIndex
end
