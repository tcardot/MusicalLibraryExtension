export dist, pcdist1, pcdist2, durdist1, durdist2, ivdist1, ivdist2, keysom, mobility, acorr,znnotes,crosscor, melaccent, melcontour

"""
    dist(data,feature,func, normalize :: Bool = true)

compute the distribution of the feature of a given data
"""
function dist(data,feature = e->e,func = e -> 1, normalize :: Bool = true)
    if iterate(data) == nothing
        return Dict()
    end
    d = Dict()
    for e in data
        f = feature(e)
        if !haskey(d,f)
            d[f] = func(e)
        else
            d[f] += func(e)
        end
    end
    sv = sum(values(d))
    if sv == 0 || !normalize
        return d
    end
    for k in keys(d)
        d[k] = d[k]/sv
    end
    return d
end

"""
    pcdist1(notes,weight = e ->1,normalize :: Bool = true)

compute the pitch-class distribution of the given notes
"""
pcdist1(notes,weight = e ->1,normalize :: Bool = true) =  dist(notes,e->pc(pitch(e)),weight,normalize)


"""
    pcdist2(notes,weight = (e1,e2)-> 1,normalize :: Bool = true)

compute the  second order pitch-class distribution of the given notes
works only with monophonic input
"""
pcdist2(notes,weight = (e1,e2)-> 1,normalize :: Bool = true) = dist(notesequence(notes,2),e->(pc(pitch(e[1])),pc(pitch(e[2]))),e->weight(e[1],e[2]),normalize)


"""
    durdist1(notes, weight = e->1,normalize :: Bool = true)

compute the duration distribution of the given notes
"""
durdist1(notes, weight = e->1,normalize :: Bool = true) = dist(notes,duration,weight,normalize)


"""
    durdist2(notes,weight = (e1,e2)-> 1,normalize :: Bool = true)

compute the second order duration distribution of the given notes
works only with monophonic input
"""
durdist2(notes,weight = (e1,e2)-> 1,normalize :: Bool = true) = dist(notesequence(notes,2),e->(duration(e[1]),duration(e[2])),e->weight(e[1],e[2]),normalize)


"""
    ivdist1(notes,weight = (e1,e2)->1,normalize :: Bool = true)

compute the interval distribution of the given notes
"""
ivdist1(notes,weight = (e1,e2)->1,normalize :: Bool = true) = dist(notesequence(notes,2),e->pitch(e[2])-pitch(e[1]),e-> weight(e[1],e[2]),normalize)


"""
    ivdist2(notes, weight = (e1,e2,e3)->1,normalize :: Bool = true)

compute the second order interval distribution of the given notes
works only with monophonic input
"""
ivdist2(notes, weight = (e1,e2,e3)->1,normalize :: Bool = true) = dist(notesequence(notes,3),e->(pitch(e[2])-pitch(e[1]),pitch(e[3])-pitch(e[2])),e->weight(e[1],e[2],e[3]),normalize)

"""
    melcontour(notes, res,func)

return an array representing the melodic contour (computed the same way as the MidiToolBox),
'func' is the function of conversion of pitches to real number.
func return value must be Float64
"""
function melcontour(notes, res,func)
    if iterate(notes) == nothing
        return []
    end
    next = iterate(notes)
    acc = Array{Float64,1}(undef,0)
    return rcontour(acc,next[1],next[2],notes,onset(next[1]),res,func)

end

"""
    rcontour(acc,note,state,notes,cur,res,func)

helper recursive function for melcontour
"""
function rcontour(acc,note,state,notes,cur,res,func)

    next = iterate(notes,state)
    ncur = cur
    if  next == nothing
        while ncur <= offset(note)
            push!(acc,func(note))
            ncur += res
        end
        return acc
    else
        if onset(next[1]) <= cur
            rcontour(acc,next[1],next[2],notes,cur,res,func)
        else

            while(ncur < onset(next[1]))
                push!(acc,func(note))
                ncur += res
            end
            rcontour(acc,next[1],next[2],notes,ncur,res,func)
        end
    end
end

"""
    acorr(notes,res,func,pairlag :: Bool = false)

return an array representing the autocorrelation of the given notes.
The Array starts with the zero-lag value.If pairlag is true,
values are paired with their corresponding lags
"""
function acorr(notes,res,func,pairlag :: Bool = false)
    if iterate(notes) == nothing
        return []
    end
    c = melcontour(notes,res,func)
    if pairlag
        return collect(zip(countfrom(0,res),autocor(c,collect(0:length(c)-1))))
    else
        return autocor(c,collect(0:length(c)-1))
    end
end



"""
    znnotes(notes,feature)

return an iterator over a zero-normalized feature of the notes
the type of the feature of the note must be compatible with mean() and stdm()
"""
function znnotes(notes,feature)
    m = mean(map(feature,notes))
    sd = stdm(map(feature,notes),m)
    return map(e->(feature(e)-m)/sd,notes)
end

"""
    keysom(notes)
"""
function keysom(notes)
    if iterate(notes) == nothing
        return []
    end
    z = zeros(24,36)
    somw = read(matopen("C:/Users/Toussain/Documents/DCML/keysomdata.mat"),"somw")
    pcd = pcdist1(notes,duraccent)

    vtmp = collect(values(pcd))
    ts = mean(vtmp)
    tm = sqrt(sum(vtmp.^2))

    for i in keys(pcd)
        pcd[i] = (pcd[i]-ts)/tm
    end
    for k = 1:36
        for l = 1:24
            s = 0.0
            for m = 1:12
                s += get!(pcd,midi(m),0)*somw[m,k,l]
            end
            z[l,k] = s
        end
    end
    return z
end


"""
    mobility(notes)

Mobility describes why melodies change direction after large skips
by simply observing that they would otherwise run out of the
comfortable melodic range. It uses lag-one autocorrelation between

"""
function mobility(notes)
    if iterate(notes) == nothing
        return []
    end
    i = 2
    p1 = Array{Float64,1}(undef, 0)
    p2 = zeros(1)
    mob = [0.0]
    y = []
    for n in notesequence(notes,2)
        m = mean(map(e->(pitch(e)).pitch,take(notes,i-1)))
        append!(p1, (pitch(n[1])).pitch - m)
        append!(p2,(pitch(n[1])).pitch - m)
        z = [p1 ;  [(pitch(n[1])).pitch - m]]
        c = cor(p2 , z)
        if isnan(c)
            push!(mob,0.0)
        else
            push!(mob, c)
        end
        push!(y,mob[i-1]*((pitch(n[2])).pitch-m))
        i += 1
    end
    y[2] = 0
    y = vcat([0],y)
    #return map(e->abs(e),y)
    return map(e->abs(e),y)
end


"""
    melaccent(notes, ivcomp = (e1,e2)-> (e1 > e2) ? 1 : ((e1 == e2) ? 0 : -1 ))

Computes melodic salience according to Thomassen's model
"""
function melaccent(notes, ivcomp = (e1,e2)-> (e1 > e2) ? 1 : ((e1 == e2) ? 0 : -1 ))
    if iterate(notes) == nothing
        return []
    end
    d = []
    i = 1
    i1 = 0.0
    i2 = 0.0
    me12 = []
    for ns in notesequence(notes,3)
        motion1 = ivcomp(pitch(ns[2]),pitch(ns[1]))
        motion2 = ivcomp(pitch(ns[3]) ,pitch(ns[2]))

        if motion1==0 && motion2==0
            (i1,i2) = (0.00001, 0.0)
        elseif motion1 !=0 && motion2==0
            (i1,i2) = (1, 0.0)
        elseif motion1==0 && motion2 !=0
            (i1,i2) = (0.00001, 1)
        elseif motion1>0 && motion2<0
            (i1,i2) = (0.83,0.17)
        elseif motion1<0 && motion2>0
            (i1,i2) = (0.71, 0.29)
        elseif motion1>0 && motion2>0
            (i1,i2) = (0.33, 0.67)
        elseif motion1<0 && motion2<0
            (i1,i2) = (0.5, 0.5)
        end

        if length(d) == 0
            push!(d,i1,i2)
        else
            d[i] = i1
            push!(d,i2)
        end

        push!(me12, (d[i],d[i+1]))
        i += 1
    end
    p2 = zeros(Float64,length(d) + 1)
    p2[1] = 1
    p2[2] = (me12[1])[1]
    for k = 3 : length(d)
        tmp1 = (me12[k-2])[2] != 0 ? (me12[k-2])[2] : 1
        tmp2 = (me12[k-1])[1] != 0 ? (me12[k-1])[1] : 1
        p2[k] = tmp1 * tmp2
        println(tmp1)
        println(tmp2)
    end

    p2[length(d) + 1] = (me12[end])[2]
    return p2
end
