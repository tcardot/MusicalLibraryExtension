module NoteToolBox

using DigitalMusicology
using DataFrames
using Ratios
using MAT
using StatsBase
using Base.Iterators

import Base: iterate, IteratorSize, IteratorEltype, length, HasLength, HasEltype

include("Tools.jl")
include("stats.jl")
end # module
