include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
include("ismonophonic.jl")
include("duraccent.jl")
include("mchannels.jl")
function ivdist1(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("null argument"))
    end
    if !ismonophonic(nframe)
        throw(ArgumentError("frame not monophonic"))
    end
    ch = mchannels(nframe)
    du = duraccent.(dur(nframe,"secs"))
    ivd = zeros(25)
    for k in ch
        d = nframe[nframe[:channel] .== k,:]
        iv = diff(Int64.(nframe[:pitch]))
        for m in 1:length(iv)
            if abs(iv[m]) <= 12
                ivd[iv[m]+ 13] += du[m+1] + du[m]
            end
        end
    end
    return ivd/(sum(ivd)+1e-12)
end


function intervalDistribution(notes, weight = e->e)
    if isempty(notes)
        throw(ArgumentError("null argument"))
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame not monophonic"))
    end

    ivd = Dict{Pitch,Any}()
    for k = 1:length(notes)-1
        iv = pitch(notes[k+1])-pitch(notes[k])
        if !haskey(ivd,iv)
            ivd[iv] = 0
        end
        ivd[iv] += duration(notes[k]) + duration(notes[k+1])
    end
    sv = sum(values(ivd))
    if sv == 0
        return ivd
    end
    for k in keys(ivd)
        ivd[k] = ivd[k]/sv
    end
    return ivd
end

#test

frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
intervalDistribution(dfToNotes(frame1,"secs"))
