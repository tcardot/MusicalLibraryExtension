include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DataFrames
using DigitalMusicology.MidiFiles

function mchannels(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end
    chn = fill(-1,16)
    foreach(e-> chn[e+1] = e, nframe[:channel])
    return filter(e-> e != -1,chn)
end


# test
#=
frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
frame1[:channel] = map(e-> 4,frame1[:channel])
for i in 1:2
   frame1[i,:channel] = 2
end
showall(frame1)
println(mchannels(frame1))
=#
