include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DataFrames

function durdist2(nframe :: DataFrame)
    if isempty(nframe)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(nframe)
        throw(ArgumentError("frame not monophonic"))
    end

    durA = dur(nframe)
    filter(e->e > 0, durA)
    durA = map(e-> round(Int64,2*log2(Float64(4*e))) + 5,durA)
    filter(e-> 1<e<9 ,durA)

    mat = zeros(Float64,9,9)
    for k in 2:length(durA )
        mat[durA[k-1],durA[k]] += 1
    end
    mat /= sum(mat)
    return mat
end

function durationDistribution2(notes, weight = e-> e)
    if isempty(notes)
        throw(ArgumentError("Null argument"))
    end
    if !ismonophonic(notes)
        throw(ArgumentError("frame not monophonic"))
    end

    d = Dict()
    for k = 1:length(notes)-1
      (dur1,dur2) = weight(duration(notes[k])), weight(duration(notes[k+1]))
      if(dur1 == 0)
        continue
      end
      if !haskey(d,(dur1,dur2))
        d[(dur1,dur2)] = 0
      end
      d[(dur1,dur2)] += 1
    end
    sv = sum(values(d))
    if sv == 0
      return d
    end
    for k in keys(d)
        d[k] = d[k]/sv
    end
    return d
end

#test
frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/laksin.mid")
durationDistribution2(dfToNotes(frame1,"secs"))
