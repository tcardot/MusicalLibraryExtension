#include("/Users/Toussain/Documents/DCML/DigitalMusicology.jl-master/src/DigitalMusicology.jl")
using DigitalMusicology
import DigitalMusicology.Pitches: midi

import DigitalMusicology.PitchOps
import DigitalMusicology.Notes
using DataFrames
using Core.Array

export noteName, ismonophonic

"""
return the name of the given MidiPitch
"""
function midiPitchName(p :: MidiPitch)
  if(p.pitch < 21 || p.pitch > 108)
    throw(ArgumentError("pitch out of bounds"))
  end

  miditable = ["C","C#","D","D#","E","F","F#","G","G#","A","A#","B"]
  return string(miditable[mod(p.pitch,12)+1],floor(Int,(p.pitch)/12)-1)
end

function ismonophonic(frame :: DataFrame)
  nrows , ncoll = size(frame)
  if ncoll != 10
    throw(ArgumentError("invalid dataFrame"))
  end

  poly = false
  index = 1

  while (!poly) && (index != nrows)

    if (frame[index][onset_ticks] == frame[index +1][onset_ticks])
      poly = true
    end
    if (frame[index][offset_ticks] > frame[index +1][onset_ticks])
      poly = true
    end
    i += 1
  end
  return !poly
end

function pcdist1(frame :: DataFrame)
  dist = fill(float(0),12)
  tdur = 0
  for row in eachrow(frame)
    dur =row[:offset_secs] - row[:onset_secs]
    dist[PitchOps.pc(row[:pitch]).pitch+1] += dur
    tdur += dur
  end
  dist = dist/tdur
  return dist
end

function onsetDist(DataFrame)
end

function


#frame1 = MidiFiles.midifilenotes("C:/Users/Toussain/Documents/DCML/MusicalLibraryExtension/functionTest/sample1.mid")
#show(pcdist1(frame1))
